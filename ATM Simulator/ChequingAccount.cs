﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ATM_Simulator
{
    class ChecquingAccount : Account
    {
        /// <summary>
        /// Declaring field variables
        /// </summary>
        public float OVERDRAFT_LIMIT = 500f;
        public float MAX_INTEREST_RATE = 1.0f;
        private float _balance;
        private float _annualIntrRate;

        public ChecquingAccount(int _acctNo, string _acctHolderName) : base(_acctNo, _acctHolderName)
        {
            _balance = 0.0f;
            _annualIntrRate = 0.0f;
        }

        // Sets the annual interest rate 
        public void setAnnualIntrRate(float newIntRate)
        {

            if (newIntRate > MAX_INTEREST_RATE)
            {
                Debug.WriteLine("ERROR CODE");
            }

            SetAnnualIntrRate(newIntRate);
        }

        //Method that withdraws the amount given
        public float withdraw(float amount)
        {

            if (amount < 0)
            {
                Debug.WriteLine("Invalid amount provided!");
            }

            if (amount > (this._balance + OVERDRAFT_LIMIT)){
                Debug.WriteLine("Isufficient funds!");
            }

            float oldBalance = this._balance;
            this._balance -= amount;
            return this._balance;
        }
    }
}
