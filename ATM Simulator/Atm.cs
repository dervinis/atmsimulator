﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ATM_Simulator
{
    /// <summary>
    /// Declaring enumeration for the simple menu choices.
    /// </summary>
    enum SimpleMenuChoices
    {
        SELECT_ACCOUNT_OPTION = 1,
        CREATE_ACCOUNT_OPTION,
        EXIT_ATM_APPLICATION_OPTION,
    }

    /// <summary>
    /// Declaring enumeration for the complex menu choices.
    /// </summary>
    enum ComplexMenuChoices
    {
        CHECK_BALANCE_OPTION = 1,
        WITHDRAW_OPTION,
        DEPOSIT_OPTION,
        EXIT_ACCOUNT_OPTION,
    }

    class Atm
    {


        public Atm()
        {
            // Creating Bank object 
            Bank bank = new Bank();

            SimpleMenuChoices choice;
            ComplexMenuChoices choice2;

        }

    }
}
