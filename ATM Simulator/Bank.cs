﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Simulator
{
    class Bank
    {
        /// <summary>
        /// Declaring field variables used for the creation of the bank object
        /// </summary>
        public int DEFAULT_ACCT_NO_START;
        private List<Account> _accountList;

        public Bank()
        {
            // Creating a new _accountList
            _accountList = new List<Account>();

            // Setting the default ID starting point
            DEFAULT_ACCT_NO_START = 100;

            // Adding 10 default accounts to the list
            for(int i = 0; i < 10; i++)
            {
                Account newDefAccount = new Account(DEFAULT_ACCT_NO_START + i, (DEFAULT_ACCT_NO_START + i).ToString());
                newDefAccount.Deposit(100.00f);
                _accountList.Add(newDefAccount);
            }
        }

        /// <summary>
        /// A method that takes account ID as an integer
        /// </summary>
        /// <param name="acctNo"></param>
        /// <returns> integer</returns>
        public int FindAccount(int acctNo)
        {

            for (int i = 0; i < this._accountList.Count; i++)
            {
                // Goes through the entire _accountList and tries to match given accNo with one of the account ID's
                if (_accountList[i].GetAccountNumber() == acctNo)
                {
                    return _accountList[i].GetAccountNumber();
                }
            }

            return 0; // If return 0 then no account has be found
        }

        //TODO: determineAccountNumber

        //TODO: openAccount

        // TODO: Public load
        // TODO:  public save

        // TODO: createDefaultAccounts()
    }
}
