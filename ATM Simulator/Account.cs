﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Simulator
{

    struct ACCOUNT_TYPE // Declaring a struct
    {
        public int ACCOUNT_TYPE_CHECQUING;
        public int ACCOUNT_TYPE_SAVINGS;
    }


    class Account
    {

        /// <summary>
        /// Declaring field variables used for the account
        /// </summary>
        private int _acctNo;
        private string _acctHolderName;
        private float _balance;
        private float _annualIntrRate;
        public ACCOUNT_TYPE accType;

        public Account(int _acctNo, string _acctHolderName)
        { // Constructor for an object
            this._acctNo = _acctNo;
            this._acctHolderName = _acctHolderName;
            _balance = 0.0f;
            _annualIntrRate = 0.0f;
        }

        /// <summary>
        /// Accessors and mutators
        /// </summary>
        public int GetAccountNumber()
        {
            return _acctNo;
        }

        public string GetAcctHolderName()
        {
            return _acctHolderName;
        }

        public float GetBalance()
        {
            return _balance;
        }

        public float GetAnnualIntrRate()
        {
            return _annualIntrRate;
        }

        public void SetAnnualIntrRate(float newIntRate)
        {
            this._annualIntrRate = newIntRate / 100;
        }

        // Deposit method that deposits the given float
        public float Deposit(float amount)
        { // Deposits money

            if (amount < 0)
            {
                // Console.WriteLine("Sorry but negative balance is not allowed!");
                return 0;
            }

            float oldBalance = this._balance;
            this._balance += amount;
            return this._balance;
        }

        public float Withdraw(float amount)
        { // Withdraws money

            if (amount < 0)
            {
                // Console.WriteLine("Sorry but invalid amount is provided!");
                return 0;
            }

            if (amount > this._balance)
            {
                // Console.WriteLine("Insufficient funds.");
                return 0;
    
            }

            float oldBalance = this._balance;
            this._balance -= amount;
            return this._balance;
        }

    }
}
