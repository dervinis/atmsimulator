﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ATM_Simulator
{
    class SavingsAccount : Account
    {
        /// <summary>
        /// Declaring field variables
        /// </summary>
        public float MATCHING_DEPOSIT_RATIO = 0.5f;
        public float MIN_INTEREST_RATE = 3.0f;
        private float _balance;
        private float _annualIntrRate;

        public SavingsAccount(int _acctNo, string _acctHolderName) : base(_acctNo, _acctHolderName)
        {
            _balance = 0.0f;
            _annualIntrRate = 0.0f;
        }

        // A method that sets annual interest rate
        public void setAnnualIntrRate(float newIntRate)
        {

            if (newIntRate < MIN_INTEREST_RATE)
            {
               Debug.WriteLine("ERROR CODE");
            }

            SetAnnualIntrRate(newIntRate);
        }

        // A method that deposits a given amount to the account
        public float deposit(float amount)
        {
            return Deposit(amount + amount * MATCHING_DEPOSIT_RATIO);
        }
    }
}
